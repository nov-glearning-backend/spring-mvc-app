package com.glearning.mvc.dao;

import java.util.Optional;
import java.util.Set;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.glearning.mvc.model.Order;


@Repository
public class OrderDAO {
	
	public Order saveOrder(Order order) {
		return null;
	}
	
	public Set<Order> fetchAll(){
		return null;
	}
	
	public Optional<Order> findOrderById(long id) {
		return null;
	}
	
	public Order updateOrder(long id, Order order) {
		return null;
	}
	
	public void deleteOrderById(long id) {
		
	}

}
