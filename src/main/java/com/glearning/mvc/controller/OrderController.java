package com.glearning.mvc.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.glearning.mvc.model.Order;
import com.glearning.mvc.service.OrderService;


@Controller
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	public Order saveOrder(Order order) {
		return this.orderService.saveOrder(order);
	}

	public Order updateOrder(long id, Order order) {
		return this.orderService.updateOrder(id, order);
	}

	public Set<Order> fetchALlOrders() {
		return this.orderService.fetchAllOrders();
	}

	public Order findOrderById(long id) {
		return this.orderService.findOrderById(id);
	}

	public void deleteOrderById(long id) {
		this.orderService.deleteOrderById(id);
	}
}
