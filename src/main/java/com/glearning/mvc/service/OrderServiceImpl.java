package com.glearning.mvc.service;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glearning.mvc.dao.OrderDAO;
import com.glearning.mvc.model.Order;


@Service
public class OrderServiceImpl implements OrderService {
	
	@Autowired
	private OrderDAO orderDAO;

	public Order saveOrder(Order order) {
		return this.orderDAO.saveOrder(order);
	}

	public Order updateOrder(long id, Order order) {
		/*
		 * Optional<Order> optionalOrder = this.orderDAO.findOrderById(id);
		 * if(optionalOrder.isPresent()) { Order existingOrder = optionalOrder.get();
		 * existingOrder.setCustomerName(order.getCustomerName());
		 * existingOrder.setOrderDate(order.getOrderDate());
		 * existingOrder.setOrderPrice(order.getOrderPrice()); return
		 * this.orderDAO.saveOrder(existingOrder); } throw new
		 * IllegalArgumentException("invalid order id passed");
		 */
		Order existingOrder = this.findOrderById(id);
		existingOrder.setCustomerName(order.getCustomerName());
		existingOrder.setOrderDate(order.getOrderDate());
		existingOrder.setOrderPrice(order.getOrderPrice());
		return this.orderDAO.saveOrder(existingOrder);
			
	}

	public Set<Order> fetchAllOrders() {
		return this.orderDAO.fetchAll();
	}

	public Order findOrderById(long id) {
		return this.orderDAO.findOrderById(id)
				.orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
	}

	public void deleteOrderById(long id) {
		this.orderDAO.deleteOrderById(id);

	}

}
