package com.glearning.mvc.service;

import java.util.Set;

import com.glearning.mvc.model.Order;

public interface OrderService {
	
	Order saveOrder(Order order);
	
	Order updateOrder(long id, Order order);
	
	Set<Order> fetchAllOrders();
	
	Order findOrderById(long id);
	
	void deleteOrderById(long id);

}
